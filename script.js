// SCRIPT.JS
// Generates pesterlogs

// this can run serverside in Node or clientside in browser
function generatePesterlog(dat, isWeb){
	var head = '<!DOCTYPE html><html><head><link rel="stylesheet" href="style.css"><title>Pester</title></head>'
	// dat passed in when run serverside
	// data is defined in tha fake `data.js` (see pester.js)
	dat = dat || data
	// format for a char is [username, textname, textColour, verb]
	// there are a few types of dialogue in Homestuck:
	// Pesterlogs are chat logs from the Pesterchum program. format:
		// -- $username [$textname] began $verbing $username [$textname] at hh:mm --
		// $textname: $line
		// ...
		// -- $username [$textname] ceased $verbing $username [$textname] at hh:mm --
	// There are also spritelogs - conversations between a player and their sprite.
		// These consist solely of
		// $textname: $line
	// Finally, dialoglogs - spoken conversation
	// in this case textname is the character's name in all caps eg
		// DAVE: $line
	var chars = [
	// beta kids
		['turntechGodhead', 'TG', 'e00707', 'pestering'],
		['tentacleTherapist', 'TT', 'b536da', 'pestering'],
		['ectoBiologist', 'EB', '0715cd', 'pestering'],
		['gardenGnostic', 'GG', '4ac925', 'pestering'],
	// all trolls that appear in a1-4
		['carcinoGeneticist', 'CG', '626262', 'trolling'],
		['grimAuxiliatrix', 'GA', '008141', 'trolling'],
		['adiosToreador', 'AT', 'a15000', 'trolling'],
		['gallowsCalibrator', 'GC', '008282', 'trolling'],
	// Sollux gets trolled by Kanaya once which kinda counts
		//['twinArmageddons', 'TA', 'a1a100', 'trolling']
	];

	// chooses two characters
	var char1 = choose(chars), char2 = choose(chars);
	// cannot pester yourself!
	while(char2[0] === char1[0]){
		char2 = choose(chars);
	}

	// timestamps for the pesterlog
	// these are displayed in hh:mm so don't need to worry about dates
	// these are all actually all on jan 1st 1970 lol
	// start time: any time of day (ie 0-24hrs in milliseconds)
	var time = randint(0, (1000 * 60 * 60 * 24)-1);
	// when the convo ends: up to 1 hour later is sensible
	var timeadd = randint(0, (1000 * 60 * 60 * 1)-1);
	// construct the date objects using `new Date(value)`
	var time1 = new Date(time)
	   ,time2 = new Date(time + timeadd);

	var pesterlog = '<p class="pesterlog">'
	// this will be if it's a pesterlog rather than, say, dialoglog
	if(char1[3]){
		pesterlog += '-- ' + char1[0] + ' [<span style="color: #' + char1[2] + '">' + char1[1] + '</span>] began '+ char1[3]+' ' + char2[0] + ' [<span style="color: #' + char2[2] + '">' + char2[1] + '</span>] at ' + padOut(time1.getHours()) + ':' + padOut(time1.getMinutes()) + ' --<br>';
	}
	// binomial probability thing
	// the probability of adding the next line is 0.95
	// so the probability of n lines would be 0.95^n
	// this provides a nice distribution for number of lines
	var p = 0.95;
	// because we don't know in advance how many lines to use
	// we start an infinite loop
	for (var i = 0;; i++) {//~ATH(THIS)
		// rolls to see if we add another line
		// when i<4, 0.95^(i-4) is greater than 1 ie we are guaranteed to roll >=4 lines
		if(Math.random() < Math.pow(p, i-4)){
			// decide who's gonna speak
			ch = choose([char1, char2]);
			// make sure the character who starts the convo has the first line
			if(i === 0){ch = char1;}
			// adds one line
			pesterlog += '<span style="color: #' + ch[2] + '">';
			pesterlog += ebubbles(ch[1], dat);
			pesterlog += '</span><br>';
		} else {
			// stops the infinite loop once we roll below threshold
			break;
		}
	}// THIS.DIE();
	// as stated above, end line of a pesterlog, if the char has a verb
	if(char1[3]){
		pesterlog += '-- ' + char1[0] + ' [<span style="color: #' + char1[2] + '">' + char1[1] + '</span>' + '] ceased '+ char1[3]+' ' + char2[0] + ' [<span style="color: #' + char2[2] + '">' + char2[1] + '</span>] at ' + padOut(time2.getHours()) + ':' + padOut(time2.getMinutes()) + ' --';
	}
	pesterlog += '</p>';
	if(isWeb){
		return pesterlog;
	} else {
		return head + '<body>' + pesterlog +
		'<div id="refresh"><a href="/">Generate another!</a></div>' +
'<div id="footer">Source code by <a href="//lbrentcarpenter.github.io">Louis Brent-Carpenter</a> &middot; <a href="mailto:garenarterius1@gmail.com">contact</a><br><a href="//mspaintadventures.com">Homestuck</a> is &copy; Andrew Hussie.<br>This non-profit website is not endorsed by or affiliated with MS Paint Adventures or What Pumpkin Studios.</div>';
	}
}

// gets a single random line of dialogue for given character
// named after 'dave_ebubbles' from act 6 intermission 3
// which is what inspired me to do this in the first place
function ebubbles(charname, text){
	// added as much crap in the regex as I can and it still sucks
	// it can't grok square brackets and dashes because even when escaped they mess up the regex
	var rx = new RegExp('(' + charname +  ': [A-Za-z0-9 &;\/\'\"\\\-=_~,.:!\?\(\)\{\}#\%*]*)(?!\.")', 'gm');
	// gets array of all of the character's lines
	//console.log(require('util').inspect(text));
	if(typeof(text) !== 'string' && text.body){text = text.body}
	var matches = text.match(rx);
	// chooses one line to use
	return choose(matches);
}

// chooses randomly an item from list
function choose(list){
	return list[randint(0, list.length)];
}

// random integer duh
function randint(min, max){
	return Math.floor(Math.random() * (max-min)) + min;
}

// is it node or web
if(typeof(exports) == 'object'){
	// Node
	exports.gen = generatePesterlog
} else {
	// web
	// adds the pesterlog on page load using jQuery because I cba to use document.getElementsById.ExtraVerboseStupidMethodIdentifier()
	$(function(){
		$('#log').html(generatePesterlog(null, true));
	});
}

// only used to make the timestamps two digits but thought I'd make a general case
// prepends/appends string s with character c until the length is n
function padOut(s, c, n, p){
	// defaults set up for the timestamps
	n = n || 2;
	c = c || '0';
	p = p || true;
	// casts to string, because Date.getHours() etc is a Number
	// and it messes up otherwise
	s = String(s);
	// pads with zeroes
	while(s.length < n){
		// prepends or appends
		if(p){s = c + s} else {s += c}
	}
	return s;
}

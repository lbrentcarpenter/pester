// PESTER.JS
// Node script that runs a server that serves up the pesterlogs

// used to read the files
var fs = require('fs');
// used to scrape the search page
var request = require('request');
// used to run the web app
var express = require('express');
var app = express();

// for generating pesterlogs serverside and serving up as static files
var getPesterlog = require('./script').gen;

var port = Number(process.env.PORT || 3000);

// scrape act 1-4 search page. Takes a while!
request('http://www.mspaintadventures.com/?search=6_1', function(err, data){
	app.get('/', function(req, res){
		res.send(getPesterlog(data));
		console.log('Sent pregenned data.');
	});
	// Sends the entire searchpage data to the client and gets it to run script.js in browser.
	// Uses more bandwidth so kinda dumb.
	/*app.get('/clientside', function(req, res){
		console.log('Sending data to clientside');
		fs.readFile('index.html', function(err, data){
			res.set('Content-Type', 'text/html');
			res.send(data);
			console.log('Sent data.');
		});
	});*/
	// necessary for clientside.
	/*app.get('/script.js', function(req, res){
		fs.readFile('script.js', function(err, data){
			res.set('Content-Type', 'text/javascript');
			res.send(data);
			console.log('Sent script.js.')
		});
	});*/
	app.get('/style.css', function(req, res){
		fs.readFile('style.css', function(err, data){
			res.set('Content-Type', 'text/css');
			res.send(data);
			console.log('Sent style.css.')
		});
	});
	// index.html has a link to this 'file'
	// which doesn't actually exist and is generated on the fly from the search page data
	app.get('/data.js', function(req, res){
		// cuts out parts we're not going to use to save bandwidth
		var str = data.body.substring(data.body.indexOf('PESTERLOG'), data.body.indexOf('END OF ACT 4'));
		// sanitize your inputs!
		str = clean(str);
		res.set('Content-Type', 'text/javascript');
		res.send('var data = \'' + str + '\'');
		console.log('Generated and sent data.js.')
	});
	// start up the server
	app.listen(port, function(){
		console.log('Server up and listening on port ' + port);
	});
});

// gets rid of line breaks and escapes quotes
function clean(string){
	string = string.replace(/([\n\f\r])+/gim, '').replace(/(['"]+)/gim, '\\$1');
	return string
}

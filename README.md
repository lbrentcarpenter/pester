# Pester

This Node application generates mock chat logs in the style of [Homestuck](http://www.mspaintadventures.com/?s=6) using lines of dialogue from its Acts 1-4.

Homestuck is (c) Andrew Hussie / What Pumpkin.